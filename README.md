# structured-cli
#### Haskell library for building structured CLI applications

This module provides the tools to build a complete "structured" CLI application, similar
to those found in systems like Cisco IOS or console configuration utilities etc. It aims
to be easy for implementors to use.

* How to use this module:

It is often the case that a simple example is the best user guide, at least for the
experienced programmer. The following code illustrates a basic but functioning CLI application:


```haskell
module Main where

import Control.Monad                 (void)
import Control.Monad.IO.Class        (liftIO)
import Data.Default                  (def)
import System.Console.StructuredCLI

root :: Commands ()
root = do
  world >+ do
    hello
    bye
    command "exit" "return to previous level" exit

world :: Commands ()
world = command "world" "enter into the world" $ return NewLevel

hello :: Commands ()
hello = command "hello" "prints a greeting" $ do
          liftIO . putStrLn $ "Hello world!"
          return NoAction

bye :: Commands ()
bye = command "bye" "say goodbye" $ do
        liftIO . putStrLn $ "Sayonara!"
        return NoAction

main :: IO ()
main = void $ runCLI "Hello CLI" def root
```

resulting example session:

```
>>> Hello CLI > ?
- world: enter into the world

>>> Hello CLI > world
>>> Hello CLI world > ?
- exit: return to previous level
- bye: say goodbye
- hello: prints a greeting

>>> Hello CLI world > hello
Hello world!
>>> Hello CLI world > bye
Sayonara!
>>> Hello CLI world > exit
>>> Hello CLI >
```

A good way to get you started is to grab the example code available under [example/Main.hs](https://gitlab.com/codemonkeylabs/structured-cli/blob/master/example/Main.hs) and modify it to suit your needs.
